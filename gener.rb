#!/usr/bin/ruby

@public_repo='./public/'

@private_repo_base='./private/'

@bin_dir='./bins/'

@names=%w[cercei tyrion jaime tywin ned aria bran]

@security_level=4


def reinit_dir (dir)
  system('rm', '-rf', "./#{dir}")
  Dir.mkdir dir
  File.chmod(0700, dir)
end



def regenerate_keys
  def spawn_publ_importer(gpg_publ_cmd)
    r, w = IO.pipe

    spawn(gpg_publ_cmd, :in => r)
    r.close

    w
  end

  def gen_key (gpg_user_cmd_base, name)
    r,w = IO.pipe

    pid = spawn(gpg_user_cmd_base + " --gen-key -", :in => r)
    r.close

    w.puts "%echo Generating #{name}"
    w.puts "Key-Type: RSA"
    w.puts "Key-Length: 2048"
    w.puts "Name-Real: #{name}"
    w.puts "Name-Email: #{name}@westeros.com"
    w.puts "%commit"

    w.close

    Process.wait pid
  end

  def export_key (gpg_user_cmd_base, sink_pipe)
    system(gpg_user_cmd_base + " --export", :out => sink_pipe)
  end
  

  reinit_dir @public_repo

  reinit_dir @private_repo_base

  sink_pipe = spawn_publ_importer "gpg --batch --homedir #{@public_repo} --import"

  @names.each do |name|
    private_repo = @private_repo_base + name

    reinit_dir private_repo

    gpg_user_cmd_base = "gpg --batch --homedir ./#{private_repo}"

    gen_key gpg_user_cmd_base, name

    export_key gpg_user_cmd_base, sink_pipe
  end

  sink_pipe.close

  Process.waitall
end

# returns an array of key IDs
def list_keys
  r,w = IO.pipe

  pid = spawn("gpg --batch --with-colons -k --homedir #{@public_repo}", :out => w)
  w.close

  key_list = []

  while r.gets
    line_data = $_.split(":")
    if line_data[0] == 'pub'
      key_list << line_data[4]
    end
  end

  r.close

  Process.wait pid

  key_list
end

def spawn_sha_sink
  r, w = IO.pipe

  spawn('sha256sum -b', :in => r)
  r.close

  w
end

def generate_bins (key_sink)
  def generate_key_chunk
    IO.read('/dev/urandom', 512)
  end

  def write_bin (number, chunk, keys)
    bin_name = @bin_dir + '%02X' % number
    receivers = keys.map { |k| "-r #{k}"}.join(' ')

    r,w = IO.pipe

    gpg_call = "gpg --batch --always-trust --homedir #{@public_repo} #{receivers} -o #{bin_name} -e"

    pid = spawn(gpg_call, :in => r)
    r.close

    w.write(chunk)

    w.close

    Process.wait pid
  end

  def combine_keys (&block)
    gap_target_count = @security_level - 1
    key_list = list_keys

    raise "Invlid security level" unless gap_target_count > 0
    raise "Security level is too high" unless gap_target_count < key_list.size

    list_keys.combination(key_list.size - gap_target_count, &block)
  end

  reinit_dir @bin_dir

  num = 0

  combine_keys do |c|
    chunk = generate_key_chunk
    key_sink.write(chunk)
    write_bin(num, chunk, c)
    num += 1
  end


  key_sink.close
  Process.waitall
end

def decrypt_key (rings)
  def list_bins
    # enumerate all bins in alphabetical order
    (Dir.entries(@bin_dir) - ['.', '..']).sort.map do |x|
      @bin_dir + x
    end
  end

  bin_list = list_bins

  chunks = bin_list.clone

  todo_count = bin_list.size

  for ring in rings
    break if todo_count <= 0

    bin_list.each_index do |bi|
      next if bin_list[bi] == nil

      r,w = IO.pipe

      pid = spawn("gpg --batch --homedir #{ring} -d #{bin_list[bi]}", :out => w, :err => '/dev/null')
      w.close

      ch = r.read

      r.close
      Process.wait pid
      if $?.exitstatus == 0
        bin_list[bi] = nil
        chunks[bi] = ch
        todo_count -= 1
      end
    end
  end

  raise "Cannot decrypt some of the bins: #{bin_list.inspect}" if todo_count > 0
 
  yield chunks.join
end

#regenerate_keys

#generate_bins (spawn_sha_sink)

decrypt_key %w{
  ./private/aria
  ./private/jaime
  ./private/cercei
  ./private/tywin
} do |key|
  sink = spawn_sha_sink
  sink.write key
  sink.close

  Process.waitall
end 
